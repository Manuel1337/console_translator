﻿using System.Text;
using System.Net;
using System.IO;
using System.Windows;
using System;

namespace Consolen_Translator
{
    class HttpHelper
    {
        

        public void GetWikipediaPage(string searchterm,string spr)
        {
            StreamReader reader = null;
            string html = null;

              string url = "http://"+spr+".wikipedia.org/wiki/"+ searchterm;
            try
            {
                var request = WebRequest.Create(url);
                var response = request.GetResponse();
                var stream = response.GetResponseStream();

                reader = new StreamReader(stream);
                html = reader.ReadToEnd();
            }
            catch (Exception e)
            {
                Console.WriteLine("Das war nichts: " + e.Message);
                return;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
          
            // Idee: finde erstes <p> und erstes </p>
            int startIndex = html.IndexOf("<p>");
            int endIndex = html.IndexOf("</p>");

          

            string ersterAbsatz = html.Substring(startIndex + 3, endIndex - startIndex - 3);
          
            StringBuilder ergebnis = new StringBuilder();
            bool copyToOutput = true;
            foreach (char c in ersterAbsatz)
            {
                if (c == '<' || c=='[')
                {
                    copyToOutput = false;
                }
                else if (c == '>' || c == ']')
                {
                    copyToOutput = true;
                    continue;
                }

                if (copyToOutput)
                    ergebnis.Append(c);
            }

            Console.WriteLine(ergebnis.ToString());
        }

        
    }
}
