﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consolen_Translator
{
    class Sprachen
    {
        //Deutsche und Englische Liste
        public List<string> Deutsch = new List<string>();
        public List<string> Englisch = new List<string>();
        public List<string> Französisch = new List<string>();
        public List<string> Irisch = new List<string>();


        public Sprachen()
        {


            FileStream fsde = new FileStream("Deutsch.txt", FileMode.Open);
            StreamReader srde = new StreamReader(fsde);

            string d = srde.ReadToEnd();
            string[] de;
            de = d.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
            foreach (var ditem in de)
            {
                if (ditem != "")
                    Deutsch.Add(ditem);
            }
            fsde.Close();
            srde.Close();




            FileStream fsen = new FileStream("Englisch.txt", FileMode.Open);
            StreamReader sren = new StreamReader(fsen);

            string e = sren.ReadToEnd();
            string[] en;
            en = e.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
            foreach (var eitem in en)
            {
                if (eitem != "")
                    Englisch.Add(eitem);
            }


            fsen.Close();
            sren.Close();

        } 

    }
}
