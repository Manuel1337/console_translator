﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Consolen_Translator
{
    
    class Datenbank
    {
        
        private MySqlConnection conn;
        public string sprache = null;
        public string wde = null;

        public Datenbank(string s,string w)
        {
            this.sprache = s;
            this.wde = w;

        }

        public List<string> GetDaten()
        {
            List<string> daten = new List<string>();
            var connString = ConfigurationManager.AppSettings["connString"];
            conn = new MySqlConnection(connString);
            try
            {
                conn.Open();
                var cmd = new MySqlCommand("Select de,"+ sprache+" from sprachen where de ='"+wde+"'", conn);

                using (var rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {

                        daten.Add(rdr.GetString(0));
                        daten.Add(rdr.GetString(1));

                    }
                }
                return daten;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return null;
            }
        }
    }
}




